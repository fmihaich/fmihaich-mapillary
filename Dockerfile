FROM python:2.7.15-alpine3.8

ENV app_dir /app

WORKDIR ${app_dir}

# Install virtualenv
RUN pip install virtualenv

# Add & Initialize Features
COPY ./system_tests ./system_tests
WORKDIR ${app_dir}/system_tests
RUN chmod +x ./run_scenarios_set_up.sh
RUN ./run_scenarios_set_up.sh

WORKDIR ${app_dir}

# Add & Initialize Server
COPY ./server ./server
WORKDIR ${app_dir}/server
RUN chmod +x ./run_server_set_up.sh
RUN ./run_server_set_up.sh

WORKDIR ${app_dir}

# Add & Initialize WebApp
COPY ./web_app ./web_app
WORKDIR ${app_dir}/web_app
RUN chmod +x ./run_web_app_set_up.sh
RUN ./run_web_app_set_up.sh

# Set Python path
ENV PYTHONPATH=${app_dir}

WORKDIR ${app_dir}/system_tests

ENTRYPOINT [ "/bin/sh", "-c"]

# Run the Features
CMD ["./run_scenarios.sh"]

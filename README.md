# The Mapillary project

# How to ...
## Run the SEVER and the WEBAPP using docker-compose
It's as simple as doing:

    docker-compose build --no-cache
    docker-compose up

And browse to: http://localhost

You'll be able to see the entire project running!

## Run scenarios

You can run all the testing scenarios by doing:

    docker-compose -f docker-compose.e2e.yml build --no-cache
    docker-compose -f docker-compose.e2e.yml up --exit-code-from system-tests

You can also execute a set of local e2e system tests that will start and stop the server,
and will assume an empty DB for each scenario. For this case the docker-compose won't rely on 
the images of the services (server and webapp), instead, it will include the actual source code 
of each one and add it into a dockerfile.

    docker-compose -f docker-compose.e2e.local.yml build --no-cache
    docker-compose -f docker-compose.e2e.local.yml up --exit-code-from system-tests

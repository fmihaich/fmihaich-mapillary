export PYTHONPATH=$PWD
export PATH=$PATH:$HOME/virtualenvs/mp-scenarios/bin
$HOME/virtualenvs/mp-scenarios/bin/behave --no-logcapture "$@"

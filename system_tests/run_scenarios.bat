@ECHO OFF
setlocal

IF EXIST \virtualenvs\mp-scenarios\scripts\activate.bat (
		call \virtualenvs\mp-scenarios\scripts\activate.bat
		IF ERRORLEVEL 1 GOTO :EOF
) ELSE (
		echo Virtual environment doesn't exist.
		echo Please run the 'run_scenarios_set_up.bat' script to create the virtualenv.
		GOTO :EOF
)

cd system_tests
set PYTHONPATH=.
behave --no-logcapture %*
cd ..

deactivate

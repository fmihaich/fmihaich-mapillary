export VIRTUALENV_FOLDER=$HOME/virtualenvs/mp-scenarios

cd set_up/
python create_bootstrap.py
python bootstrap.py $VIRTUALENV_FOLDER

echo Virtualenv for running the SYSTEM TESTS of MAPILLARY EXERCISE was correctly created - $VIRTUALENV_FOLDER

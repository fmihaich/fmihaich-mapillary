import json
import logging
import requests

from assertpy import assert_that
from behave import step
from random import randint

from features.steps.utils.user_mgmt import get_user_from_context, DEFAULT_USER_ID


SERVER_ADD_USER_URL = '{server_url}/user/add'
HTTP_CREATED = 201


@step(u'I add a user')
@step(u'I add "{user_id}" user')
def add_user(context, user_id=DEFAULT_USER_ID):
    user = get_user_from_context(context, user_id)
    logging.info('Adding user which id is: "{0}"'.format(user_id))
    add_user_response = _perform_add_user_request(server_url=context.server_url, user_data=json.dumps(user))
    assert_that(add_user_response.status_code).is_equal_to(HTTP_CREATED)


@step(u'I add "{user_count:d}" users')
def add_users(context, user_count):
    user_id_prefix = 'user_{0}'.format(randint(0, 1000000))
    for i in range(1, user_count+1):
        user_id = user_id_prefix + '_{0}'.format(i)
        add_user(context, user_id)


@step(u'I can not add other user which "{user_data}" is the same than "{base_user_id}" username')
def add_user_again_with_specific_data(context, user_data, base_user_id):
    new_user_id = 'new_user_{0}'.format(randint(0, 1000000))
    new_user = get_user_from_context(context, user_id=new_user_id)
    base_user = get_user_from_context(context, user_id=base_user_id)
    new_user[user_data] = base_user[user_data]

    logging.info('Adding user which id is: "{0}"'.format(new_user_id))
    add_user_response = _perform_add_user_request(server_url=context.server_url, user_data=json.dumps(new_user))
    assert_that(add_user_response.status_code).is_not_equal_to(HTTP_CREATED)


@step(u'I try to add a user without specifying the "{missing_data}"')
def add_user_with_missing_data(context, missing_data):
    user = get_user_from_context(context)
    del user[missing_data]
    logging.info('Trying to add a user without "{0}"'.format(missing_data))
    _perform_add_user_request(server_url=context.server_url, user_data=json.dumps(user))


@step(u'I try to add a user with incomplete data')
def add_user_with_incomplete_data(context):
    user = get_user_from_context(context)
    incomplete_user_data = json.dumps(user)[:-3]
    logging.info('Trying to add a user with incomplete data')
    _perform_add_user_request(server_url=context.server_url, user_data=incomplete_user_data)


def _perform_add_user_request(server_url, user_data):
    request_url = SERVER_ADD_USER_URL.format(server_url=server_url)
    logging.info('Add user url: "{0}"'.format(request_url))
    logging.info('Add user body: "{0}"'.format(user_data))

    try:
        response = requests.post(request_url, data=user_data, headers={"Content-Type" : "application/json"})
    except Exception as e:
        logging.exception('Add user request error:\n{0}'.format(str(e)))
        return False

    logging.info('Add user response: "{0}"'.format(response))
    return response

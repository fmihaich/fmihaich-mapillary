import logging
import os

from behave import step
from random import randint


@step(u'local DB is empty')
def create_empty_db(context):
    if context.suite_execution_type == 'DOCKER':
        logging.warning(
            'Assuming server running with an already created DB when suite execution type is "docker". Nothing to do')
    else:
        context.db_path = os.path.join(context.scenario_dir, 'server_db_{0}.json'.format(randint(0, 1000000)))
        open(context.db_path, 'a').close()
        logging.info('Using the following DB path: "{0}"'.format(context.db_path))


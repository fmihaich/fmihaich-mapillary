import logging

from assertpy import assert_that
from behave import step

from features.steps.utils.user_mgmt import get_user_from_context, DEFAULT_USER_ID


@step(u'No user is shown')
def check_no_user_is_shown(context):
    shown_users = _get_all_shown_users(context)
    assert_that(shown_users).is_length(0)


@step(u'the user is shown')
@step(u'the recent submitted user is shown')
def check_user_is_shown(context):
    _check_multiple_users_are_shown(context, user_ids=[DEFAULT_USER_ID])


@step(u'all the users are shown')
def check_all_users_are_shown(context):
    _check_multiple_users_are_shown(context, user_ids=context.users.keys())


@step(u'the user is not shown')
def check_user_is_not_shown(context):
    shown_users = _get_all_shown_users(context)
    expected_user = get_user_from_context(context)
    logging.info('Expected user to be not shown: "{0}"'.format(expected_user))
    assert_that(shown_users).does_not_contain(expected_user)


def _check_multiple_users_are_shown(context, user_ids):
    shown_users = _get_all_shown_users(context)
    for user_id in user_ids:
        expected_user = get_user_from_context(context, user_id)
        logging.info('Expected user: "{0}"'.format(expected_user))
        assert_that(shown_users).contains(expected_user)


def _get_all_shown_users(context):
    all_users_page = context.current_page
    shown_users = all_users_page.get_all_users()
    logging.info('Shown users: "{0}"'.format(shown_users))
    return shown_users

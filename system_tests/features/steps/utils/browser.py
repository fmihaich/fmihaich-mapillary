from os import environ
from selenium import webdriver

WEBDRIVER_REMOTE_HOST = environ['WEBDRIVER_REMOTE_HOST'] if 'WEBDRIVER_REMOTE_HOST' in environ else ''

class Browser(object):

    def __init__(self):
        if (WEBDRIVER_REMOTE_HOST != ''):
            command_executor = 'http://' + WEBDRIVER_REMOTE_HOST + ':4444/wd/hub'
            self.driver = webdriver.Remote(desired_capabilities=webdriver.DesiredCapabilities.CHROME,
                                           command_executor=command_executor)
        else:
            self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(2)

    def close(self):
        self.driver.close()

    def go_to(self, url):
        self.driver.get(url)
        return self.driver

    def refresh(self):
        self.driver.refresh()

import logging
import time
import os

from assertpy import assert_that
from behave import step

from features.steps.page_objects.web_app_page import WebAppPage
from features.steps.utils.browser import Browser

DEFAULT_WEB_APP_HOST = 'localhost'
DEFAULT_WEB_APP_PORT = '5000'


@step(u'the web app is opened')
def open_web_app(context):
    web_app_host = os.environ['WEBAPP_HOST'] if 'WEBAPP_HOST' in os.environ else DEFAULT_WEB_APP_HOST
    web_app_port = os.environ['WEBAPP_PORT'] if 'WEBAPP_PORT' in os.environ else DEFAULT_WEB_APP_PORT

    context.browser = Browser()
    context.cleanup_tasks.append(_close_browser)

    web_app_url = 'http://{host}:{port}'.format(host=web_app_host, port=web_app_port)
    logging.info('Web app url: "{0}"'.format(web_app_url))
    context.browser.go_to(url=web_app_url)
    time.sleep(2)

    context.current_page = WebAppPage(context.browser.driver)
    context.cleanup_tasks.append(_close_browser)


@step(u'I click on "{menu_option}" option')
def click_on_menu_option(context, menu_option):
    web_app_page = context.current_page
    web_app_page.select_menu_option(menu_option)


@step(u'I see a "{expected_feedback}" feedback message')
def check_input_feedback(context, expected_feedback):
    web_app_page = context.current_page
    current_feedback = web_app_page.get_feedback_message()
    logging.info('Current feedback: {0}'.format(current_feedback))
    assert_that(current_feedback).contains_ignoring_case(expected_feedback)


@step(u'I refresh the page')
def refresh_current_page(context):
    context.browser.refresh()
    time.sleep(1)


def _close_browser(context):
    if not hasattr(context, 'browser'):
        try:
            context.browser.close()
        except Exception as e:
            logging.warning('Error closing the browser: "{0}"'.format(e))

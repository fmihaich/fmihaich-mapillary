import logging
import shutil
import os
import platform
import subprocess
import time

from behave import step
from configobj import ConfigObj
from random import randint


@step(u'the server is up and running')
def start_server(context):
    if context.suite_execution_type == 'DOCKER':
        server_host = os.environ['SERVER_HOST'] if 'SERVER_HOST' in os.environ else '127.0.0.1'
        server_port = os.environ['SERVER_PORT'] if 'SERVER_PORT' in os.environ else '8080'
        server_url = 'http://{host}:{port}'.format(host=server_host, port=server_port)
        logging.info('Assuming server is running in "{0}" due to suite execution type is "docker"'.format(server_url))
    else:
        logging.info('Initializing server locally...')
        server_url = _initialize_server_locally(context)

    context.server_url = server_url


@step(u'the server is up and running again')
def re_start_server(context):
    if context.suite_execution_type == 'DOCKER':
        logging.info('Assuming server already running when suite execution type is "docker". Nothing to do')
    else:
        _run_server(context)
        if stop_server not in context.cleanup_tasks:
            context.cleanup_tasks.append(stop_server)


@step(u'the server goes down')
def stop_server(context):
    if context.suite_execution_type == 'DOCKER':
        logging.info('The server shall never be stopped when suite execution type is "docker". Nothing to do')
    if hasattr(context, 'server'):
        logging.info('Stopping the server...')
        context.server.kill()
        time.sleep(2)


def _initialize_server_locally(context):
    context.server_config = _create_test_server_config(context)
    logging.info('Using the following configuration to run the server: "{0}"'.format(context.server_config))

    _run_server(context)

    server_host = ConfigObj(context.server_config, file_error=True)['server']['host']
    if server_host == '0.0.0.0':
        server_host = 'localhost'
    server_port = ConfigObj(context.server_config, file_error=True)['server']['port']
    context.cleanup_tasks.append(stop_server)

    return 'http://{host}:{port}'.format(host=server_host, port=server_port)


def _create_test_server_config(context):
    test_config_path = _copy_base_server_config_to_temp_folder(context)
    test_config = ConfigObj(test_config_path, file_error=True)

    _set_config_attr(context, test_config, attr='db_path', config_section='db', config_key='path')

    context.server_log = os.path.join(context.scenario_dir, 'server_log.log')
    _set_config_attr(context, test_config, attr='server_log', config_section='logs', config_key='path')

    test_config.write()
    return test_config_path


def _copy_base_server_config_to_temp_folder(context):
    test_config_path = os.path.join(context.scenario_dir, 'test_config_{0}.cfg'.format(randint(0, 1000000)))
    shutil.copy(os.path.abspath(context.suite_config['server']['base_cfg_path']), test_config_path)

    return test_config_path


def _set_config_attr(context, base_config, attr, config_section, config_key):
    if hasattr(context, attr):
        base_config[config_section][config_key] = getattr(context, attr)


def _run_server(context):
    virtualenv_folder = context.suite_config['server']['virtualenv_folder']
    server_path = os.path.abspath(context.suite_config['server']['path'])

    if 'WINDOWS' in platform.system().upper():
        python_interpreter = os.path.abspath('C:\\virtualenvs\\{0}\\Scripts\\python.exe'.format(virtualenv_folder))
        shell_option = False
    else:
        python_interpreter = \
            os.path.abspath('{0}/virtualenvs/{1}/bin/python'.format(os.path.expanduser('~'), virtualenv_folder))
        shell_option = True

    run_server_command = "{0} {1} --config {2}".format(python_interpreter, server_path, context.server_config)
    logging.info("Starting server: {0}".format(run_server_command))
    context.server = subprocess.Popen(run_server_command, shell=shell_option)
    
    time.sleep(2)

import logging
import os
import platform
import subprocess
import time

from behave import step


WEB_APP_FEATURE_VAR = 'web_app'


@step(u'the web app is up and running')
def start_web_app(context):
    if context.suite_execution_type == 'DOCKER':
        logging.info('Assuming web app is already running when suite execution type is "docker". Nothing to do')
    else:
        if WEB_APP_FEATURE_VAR in context.feature_variables:
            logging.info('Web app is already running. Skipping...')
        else:
            _run_web_app(context)
            context.feature_cleanup_tasks.append(_stop_web_app)


def _run_web_app(context):
    virtualenv_folder = context.suite_config['web_app']['virtualenv_folder']
    web_app_path = os.path.abspath(context.suite_config['web_app']['path'])

    if 'WINDOWS' in platform.system().upper():
        python_interpreter = os.path.abspath('C:\\virtualenvs\\{0}\\Scripts\\python.exe'.format(virtualenv_folder))
        shell_option = False
    else:
        python_interpreter = \
            os.path.abspath('{0}/virtualenvs/{1}/bin/python'.format(os.path.expanduser('~'), virtualenv_folder))
        shell_option = True

    run_web_app_command = "{0} {1}".format(python_interpreter, web_app_path)
    logging.info("Starting web app: {0}".format(run_web_app_command))

    context.feature_variables[WEB_APP_FEATURE_VAR] = subprocess.Popen(run_web_app_command, shell=shell_option)
    time.sleep(2)


def _stop_web_app(context):
    if WEB_APP_FEATURE_VAR in context.feature_variables:
        logging.info('Stopping the web app...')
        context.feature_variables[WEB_APP_FEATURE_VAR].kill()

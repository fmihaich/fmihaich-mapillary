import json
import logging
import requests

from assertpy import assert_that
from behave import step


from features.steps.utils.user_mgmt import get_user_from_context, DEFAULT_USER_ID


SERVER_GET_USERS_URL = '{server_url}/users'
HTTP_OK = 200


@step(u'I get all stored users')
def get_users(context):
    request_url = SERVER_GET_USERS_URL.format(server_url=context.server_url)
    logging.info('Get users url: "{0}"'.format(request_url))
    try:
        get_users_response = requests.get(request_url)
        assert_that(get_users_response.status_code).is_equal_to(HTTP_OK)
        context.stored_users = json.loads(get_users_response.content)
        logging.info('Get users response content: "{0}"'.format(context.stored_users))
    except Exception as e:
        error_msg = 'Error getting users\n{0}'.format(str(e))
        logging.exception(error_msg)
        assert False, error_msg


@step(u'I can not get stored users')
def check_server_unreachable(context):
    request_url = SERVER_GET_USERS_URL.format(server_url=context.server_url)
    logging.info('Get users url: "{0}"'.format(request_url))
    try:
        requests.get(request_url)
    except Exception as e:
        logging.info('Connection error as it was expected:\n{0}'.format(str(e)))
        return

    assert False, 'Connection error was expected when getting all stored users.'


@step(u'No user is returned')
def check_no_user_is_returned(context):
    assert_that(context.stored_users).is_empty()


@step(u'the user is returned')
def check_user_is_returned(context, user_id=DEFAULT_USER_ID):
    user = get_user_from_context(context, user_id)
    _assert_exactly_count_of_expected_users(users=context.stored_users, expected_info=user, count=1)


@step(u'the user is not returned')
def check_user_is_not_returned(context):
    user = get_user_from_context(context)
    _assert_exactly_count_of_expected_users(users=context.stored_users, expected_info=user, count=0)


@step(u'all the recent created users are returned')
def check_all_users_are_returned(context):
    for user_id in context.users.keys():
        check_user_is_returned(context, user_id)


@step(u'I see only "{user_count:d}" user with "{user_id}" "{user_data}"')
def check_exactly_stored_user_data(context, user_count, user_id, user_data):
    expected_user_data = get_user_from_context(context, user_id)
    expected_info = {user_data: expected_user_data[user_data]}
    _assert_exactly_count_of_expected_users(users=context.stored_users, expected_info=expected_info, count=user_count)


def _assert_exactly_count_of_expected_users(users, expected_info, count):
    user_with_expected_info = _get_user_with_expected_data(users, expected_info)
    logging.info('Users with expected info: "{0}'.format(user_with_expected_info))
    assert_that(user_with_expected_info).is_length(count)


def _get_user_with_expected_data(users, expected_info):
    return [user for user in users if all(user[data] == expected_info[data] for data in expected_info.keys())]

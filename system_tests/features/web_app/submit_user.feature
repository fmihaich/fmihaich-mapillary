Feature: Submit user

Background:
  Given local DB is empty
  And the server is up and running
  And the web app is up and running
  And the web app is opened

@UI @GET_USERS
Scenario: No user is shown when no user was previously submitted
  When I click on "All Users" option
  Then No user is shown

@DOCKER @UI @GET_USERS @ADD_USER @SMOKE
Scenario: A successfully submitted user is shown in all user tab
  Given I click on "New User" option
  When I complete all user information for a user
  And I click on submit user button
  Then I see a "success" feedback message
  When I click on "All Users" option
  Then the recent submitted user is shown

@DOCKER @UI @GET_USERS @ADD_USER
Scenario: Multiple successfully submitted users are shown in all user tab
  Given I click on "New User" option
  And I submit "3" users
  When I click on "All Users" option
  Then all the users are shown

@UI @GET_USERS @ADD_USER
Scenario: A user is shown after a server re-connection
  Given I click on "New User" option
  And I submit a user
  And I see a "success" feedback message
  When the server goes down
  And I click on "All Users" option
  Then I see a "Warning" feedback message
  And No user is shown
  When the server is up and running again
  And I refresh the page
  Then the user is shown

@DOCKER @UI @ADD_USER
Scenario: A user which birthday is incorrect is not submitted
  Given I click on "New User" option
  When I complete all user information for a user which "birthday" is "25/25/1995"
  And I click on submit user button
  Then I see a "Warning" feedback message
  When I click on "All Users" option
  Then the user is not shown

@DOCKER @UI @ADD_USER
Scenario: A user which information is incomplete is not submitted
  Given I click on "New User" option
  When I complete all user information except "surname"
  And I click on submit user button
  Then I see a "Warning" feedback message
  When I click on "All Users" option
  Then the user is not shown
import logging
import os
import shutil

from configobj import ConfigObj


def before_all(context):
    CONFIG_PATH = os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.cfg'))
    context.suite_config = ConfigObj(CONFIG_PATH, file_error=True)
    context.suite_execution_type = \
        os.environ['SUITE_EXECUTION_TYPE'] if 'SUITE_EXECUTION_TYPE' in os.environ else 'LOCAL'

    output_dir = os.path.abspath(context.suite_config['output']['path'])
    _create_directory(dir_path=output_dir)

    log_path = os.path.join(output_dir, context.suite_config['log']['file_name'])
    context.config.setup_logging(filename=log_path,
                                 format='%(asctime)s [%(levelname)s] - %(message)s',
                                 level=context.suite_config['log']['level'])

    logging.info('-------------------- INITIALIZING TEST SUITE --------------------\n')
    logging.info('Using the following suite execution type: "{0}"'.format(context.suite_execution_type))


def before_feature(context, feature):
    logging.info('-------------------- BEFORE FEATURE --------------------')
    logging.info('Scenario: {0}'.format(feature.name))
    logging.info('---------------------------------------------------------\n')
    context.feature_variables = {}
    context.feature_cleanup_tasks = []


def before_scenario(context, scenario):
    logging.info('-------------------- BEFORE SCENARIO --------------------')
    logging.info('Scenario: {0}'.format(scenario.name))
    logging.info('---------------------------------------------------------\n')

    scenario_dir_name = scenario.name.replace(' ', '_')
    context.scenario_dir = os.path.join(os.path.abspath(context.suite_config['output']['path']), scenario_dir_name)
    _create_directory(dir_path=context.scenario_dir)

    context.users = {}
    context.cleanup_tasks = []

    if context.suite_execution_type == 'DOCKER' and 'DOCKER' not in scenario.effective_tags:
        scenario.skip("Skipping this scenario due to it is not docker compliance")


def after_scenario(context, scenario):
    for task in context.cleanup_tasks:
        try:
            task(context)
        except Exception:
            logging.exception('Error executing cleanup task')

    logging.info('---------------------------------------------------------\n')


def after_feature(context, feature):
    for task in context.feature_cleanup_tasks:
        try:
            task(context)
        except Exception:
            logging.exception('Error executing feature cleanup task')

    logging.info('---------------------------------------------------------\n')

def _create_directory(dir_path):
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path, ignore_errors=True)
    os.makedirs(dir_path)

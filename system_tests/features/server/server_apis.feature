Feature: Server APIs

Background:
  Given local DB is empty
  And the server is up and running

@API @SYSTEM @GET_USERS
Scenario: No user is returned when no user was previously stored
  When I get all stored users
  Then No user is returned

@DOCKER @API @SYSTEM @GET_USERS @ADD_USER @SMOKE
Scenario Outline: All stored users are returned when they were previously added
  Given I add "<user_count>" users
  When I get all stored users
  Then all the recent created users are returned
  Examples:
    | user_count |
    | 1          |
    | 3          |

@API @SYSTEM @GET_USERS @ADD_USER
Scenario: A stored user can be obtained after server re-connection
  Given I add a user
  When the server goes down
  Then I can not get stored users
  When the server is up and running again
  And I get all stored users
  Then the user is returned

@DOCKER @API @SYSTEM @ADD_USER
Scenario: Only one user per username is stored
  When I add "user_1" user
  Then I can not add other user which "username" is the same than "user_1" username
  When I get all stored users
  Then I see only "1" user with "user_1" "username"

@DOCKER @API @SYSTEM @ADD_USER
Scenario: User without a mandatory attribute is not stored
  Given I try to add a user without specifying the "email"
  When I get all stored users
  Then the user is not returned

@DOCKER @API @SYSTEM @ADD_USER
Scenario: User with incomplete data is not stored
  Given I try to add a user with incomplete data
  When I get all stored users
  Then the user is not returned

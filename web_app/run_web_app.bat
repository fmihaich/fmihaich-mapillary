@ECHO OFF
setlocal

IF EXIST \virtualenvs\mp-web-app\scripts\activate.bat (
		call \virtualenvs\mp-web-app\scripts\activate.bat
		IF ERRORLEVEL 1 GOTO :EOF
) ELSE (
		echo Virtual environment doesn't exist.
		echo Please run the 'run_web_app_set_up.bat' script to create the virtualenv.
		GOTO :EOF
)

set PYTHONPATH=%~dp0web_app\

echo %PYTHONPATH%


python %~dp0web_app\app.py

deactivate
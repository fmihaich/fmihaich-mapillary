PUSHD %~dp0
@ECHO OFF

set VIRTUALENV_FOLDER=\virtualenvs\mp-web-app

cd set_up/
python create_bootstrap.py
python bootstrap.py %VIRTUALENV_FOLDER%

echo Virtualenv for running the MAPILLARY WEB APP was correctly created - $VIRTUALENV_FOLDER

:END
POPD

# WebApp
## Prerequisites
- Have docker installed
- Have the API server up and running. Check [here](../server/README.md) how-to.

## Start the server
Build the image:
    
    docker build --tag mp-web-app:1.0 .

You should check your local IP address (i.e: 192.168.1.11) then start the image in interactive mode (so you can see console output while running):

    docker run -e SERVER_HOST=192.168.1.11 -p 5000:5000 -ti mp-web-app:1.0

Browse to http://localhost:5000 and you'll see the app up and running! Add user, browse for users! The two containers are connected and should be working smoothly. 

Notice: see [global](../README.md) file for checking how to run both services simultaneously using `docker-compose`.

@ECHO OFF
setlocal

IF EXIST \virtualenvs\mp-server\scripts\activate.bat (
		call \virtualenvs\mp-server\scripts\activate.bat
		IF ERRORLEVEL 1 GOTO :EOF
) ELSE (
		echo Virtual environment doesn't exist.
		echo Please run the 'run_server_set_up.bat' script to create the virtualenv.
		GOTO :EOF
)

set PYTHONPATH=%~dp0server\src

echo %PYTHONPATH%


python %~dp0server\src\server.py

deactivate
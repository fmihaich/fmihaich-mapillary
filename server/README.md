# Server
## Prerequisites
- Have docker installed

## Start the server
Build the image:
    
    docker build --tag mp-server:1.0 .

Start the image in interactive mode (so you can see console output while running):

    docker run -p 8080:8080 -ti mp-server:1.0

### Check the API is working

Do a **GET** to http://127.0.0.1:8080/users you should receive an empty array.

Do a **POST** to http://127.0.0.1:8080/user/add with body:

    {
        "username": "tester",
        "name": "Tester",
        "surname": "Guy",
        "email": "tester_guy@yopmail.com",
        "birthday": "01/11/2000",
        "address": "111 Some st"
    }

You should get a **201 (Created)**. If you now try to list all users (GET mentioned before) you'll see the user in the list.

## Run unit tests
Run the docker image with:

    docker run -ti mp-server:1.0 ./run_server_unit_tests.sh

You'll see the unit tests running and coverage report in the console.

""" This module create file bootstrap.py
"""
import virtualenv
import textwrap


def create_bootstrap():
    """Create bootstrap file"""
    output = virtualenv.create_bootstrap_script(textwrap.dedent("""
    import os, subprocess, shutil, platform

    bin_folder = 'bin'
    if platform.system() == "Windows":
        bin_folder = 'Scripts'

    def after_install(options, virtualenv_dir):
        \"\"\"Prepare environment for running MeLi interview exercises\"\"\"
        install_python_packages(virtualenv_dir)
        pip = join(virtualenv_dir, bin_folder, 'pip')
        process = subprocess.Popen([pip, "freeze"], stdout=subprocess.PIPE)
        installed_libraries = process.communicate()[0].lower()

    def install_python_packages(virtualenv_dir):
        \"\"\"Install libraries\"\"\"
        pip = join(virtualenv_dir, bin_folder, 'pip')
        install_requirements(virtualenv_dir, pip)

    def install_requirements(virtualenv_dir, pip):
        \"\"\"Install requirements\"\"\"
        with open('requirements.txt', 'r') as f:
            for requirement in f.readlines():
                print("-------------------------------------")
                print("Installing " + requirement)
                print("-------------------------------------")
                subprocess.call([pip, 'install', '--upgrade', requirement])
    """))
    with open('bootstrap.py', 'w') as file_bootstrap:
        file_bootstrap.write(output)


if __name__ == '__main__':
    create_bootstrap()

find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
export PYTHONPATH=$PWD/server/src
$HOME/virtualenvs/mp-server/bin/py.test --cov-report term-missing --cov-branch --cov=./server/src ./server/test
